function onPageLoad() {
    // on page load, go through and figure out what state we should be in
    // once in the right state, all state transitions are handled gracefully 
    // if homepage, set appropriate elements
    if(isMainPage()) {

    }
    // else if specific lobby, get some more info
    else {
      // are we an active user or should we show username prompt?
      if(!isUserRegistered()) {
        // user is NOT yet registered
        if(isSetupPeriod()) {
          // only if we are in setup period, give ability to enter username
          // open box to ask for username and set to "waiting for username" state
        }
        else {
          // show error that you can't join this match
        }
      }
      else {
        // user IS already registered in this lobby
        // display 
        
      }
    }
  }

  function updateView() {
    // update CSS/HTML according to state/flags
    // only call this if the state actually changed!
    setElementVisibility('loading', isLoading);
    setElementVisibility('homepage', displayState == 'homepage');
    setElementVisibility('in-match', displayState == 'in-match');
    if(displayState == 'in-match') {
      if(!gpsHasLoaded) {
        gpsHasLoaded = true;
        displayedMap.startUpdatingPosition();
      }
      if(isHider) {
        document.getElementById('hud-hider').style.display = 'flex';
        document.getElementById('role-title').innerHTML = 'hide';
      }
      else {
        document.getElementById('hud-hider').style.display = 'none';
        document.getElementById('role-title').innerHTML = 'seek';
      }
      setElementVisibility('hud-hider', isHider == true);
    }
  }

  class ActiveGpsMap {
    constructor() {
      // Where you want to render the map.
      this.element = document.getElementById('in-match-map');
      // Create Leaflet map on map element.
      this.map = L.map(this.element);
      //
      var pointIcon = L.icon({
        iconUrl: 'seeker_map.svg',
        //shadowUrl: 'leaf-shadow.png',
        iconSize:     [32, 32], // size of the icon
        //shadowSize:   [50, 64], // size of the shadow
        iconAnchor:   [16, 16], // point of the icon which will correspond to marker's location
        //shadowAnchor: [4, 62],  // the same for the shadow
        popupAnchor:  [16, 16] // point from which the popup should open relative to the iconAnchor
      });
      // Cluster of markers that are shown to user
      // this.markers = L.markerClusterGroup();
      // Add OSM tile layer to the Leaflet map.
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(this.map);
      // Set initial view
      this.map.setView(L.latLng(0,0), 14);
      // Special marker for user's current location, so we can update separately
      this.userMarker = L.marker(L.latLng(0,0), {icon: pointIcon});
      // Update location of user marker
      // get the user's location and center the map to that the first time
      this.updateUserLocationOnMap(true);
      this.userMarker.addTo(this.map);

    }

    startUpdatingPosition() {
      // Call getLocation() every 5 seconds but don't center on these
      var me = this;
      var intervalId = setInterval(function(){me.updateUserLocationOnMap(false)}, 3000);
    }

    updateUserLocationOnMap(doCenter = false) {
      var self = this;
      this.getLocation(function(position){
        self.updateMarkerPosition(self.map, self.userMarker, position, doCenter)
      });
    }

    getLocation(callback) {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(callback);
      }
      // else geolocation is not supported TODO
    }
    updateMarkerPosition(map, marker, position, doCenter) {
      var target = L.latLng(position.coords.latitude, position.coords.longitude)
      marker.setLatLng(target);
      // alert(this.userMarker.getLatLng());
      if(doCenter) {
        map.setView(target, map.getZoom());
      }
      //
      // Update markers
      // layerGroup.clearLayers();
      //L.marker(target).addTo(map);
    }
  }


  
  function setElementVisibility(element, doShow) {
    if(doShow) {
        document.getElementById(element).style.visibility = 'visible';
    }
    else {
        document.getElementById(element).style.visibility = 'hidden';
    }
  }

  function clickCreateLobby() {
    // Display loading icon
    // Here is where you would ask the backend for a new lobby ID
    // For now, placeholder
    thisLobby = makeid(8);
    alert("New lobby code: " + thisLobby);
  }
  function clickJoinLobby() {
    alert(document.getElementById('lobby-code').value);
    displayState = 'in-match'
    updateView();
  }

  function onTaggedPress() {
    isHider = false;
    updateView();
  }