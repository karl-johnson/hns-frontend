function makeid(length) {
    let result = '';
    const characters = 'abcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
}

function isMainPage() {
    // TODO are we loading the main page?
}

function isUserRegistered() {
    // TODO is this client/session token registered in the lobby?
}

function isSetupPeriod() {
    // TODO is the lobby in the setup period?
}